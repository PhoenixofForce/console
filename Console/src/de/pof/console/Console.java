package de.pof.console;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public abstract class Console extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private JTextField userInputs;
	private JTextPane outputs;
	private JScrollPane scrollpane;
	
	private Insets i;
	
	private List<String> usedCommands;
	private int commandIndex = 0;
	
	private String lastInput = "";
	
	public Console(){
		
		this.setTitle("POF-CONSOLE");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setVisible(true);
		i = getInsets();
		this.setSize(500 + i.left + i.right, 600 + i.top + i.bottom);
				
		userInputs = new JTextField();
		outputs = new JTextPane();
		outputs.setEditable(false);
		usedCommands = new ArrayList<>();
		scrollpane = new JScrollPane(outputs);		
		
		this.add(userInputs);
		this.add(scrollpane);
		
		addListener();
		resize();
	}
		
	public abstract void onLineWrite();
	
	private void resize(){
		
		int width = this.getWidth() - i.left - i.right;
		int height = this.getHeight() - i.top - i.bottom;
		
		outputs.setBounds(5, 5, width - 10, height - 50);
		scrollpane.setBounds(5, 5, width - 10, height - 50);
		userInputs.setBounds(5, height - 40, width - 10, 35);
		
	}
	
	private void addListener(){
		this.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				
				resize();
				
            }
        });
		
		userInputs.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_UP){
					if(usedCommands.size() > 0){
						
						commandIndex = Math.max(0, commandIndex - 1);
						userInputs.setText(usedCommands.get(commandIndex));
						
					}
				}else if(e.getKeyCode() == KeyEvent.VK_DOWN){
					if(usedCommands.size() > 0){
						
						commandIndex = Math.min(usedCommands.size(), commandIndex + 1);
						userInputs.setText(commandIndex == usedCommands.size()? "": usedCommands.get(commandIndex));
						
					}
				}
			}
		});
		
		userInputs.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if(userInputs.getText().length() > 0){
				
					String command = userInputs.getText();
					userInputs.setText("");
					addOutput(">> " + command).output();
					
					usedCommands.add(command);
					commandIndex = usedCommands.size();			
										
					lastInput = command;
					onLineWrite();
				}
			}
		});
	}
	
	public Console addOutput(String out){
		StyledDocument doc = outputs.getStyledDocument();

        Style style = outputs.addStyle("", null);
        StyleConstants.setForeground(style, Color.BLACK);

        try { doc.insertString(doc.getLength(), out, style); }
        catch (Exception e){}
        
        return this;
	}
	
	public Console addColoredOutput(String out, Color c){
		StyledDocument doc = outputs.getStyledDocument();

        Style style = outputs.addStyle("", null);
        StyleConstants.setForeground(style, c);

        try { doc.insertString(doc.getLength(), out,style); }
        catch (Exception e){}
        return this;
	}
	
	public Console addHighlightedOutput(String out, Color c){
		StyledDocument doc = outputs.getStyledDocument();

        Style style = outputs.addStyle("", null);
        StyleConstants.setForeground(style, c);
        StyleConstants.setBackground(style, c.darker());

        try { doc.insertString(doc.getLength(), out,style); }
        catch (Exception e){}
        return this;
	}
	
	public Console addHighlightedColoredOutput(String out, Color back, Color front){
		StyledDocument doc = outputs.getStyledDocument();

        Style style = outputs.addStyle("", null);
        StyleConstants.setForeground(style, front);
        StyleConstants.setBackground(style, back);
        
        try { doc.insertString(doc.getLength(), out,style); }
        catch (Exception e){}
        return this;
	}
	
	public Console addImage(String filePath){
		StyledDocument document = outputs.getStyledDocument();
		
		Style labelStyle = outputs.addStyle("", null);
		
		Icon icon = new ImageIcon(filePath);
		JLabel label = new JLabel(icon);
		StyleConstants.setComponent(labelStyle, label);
		
		try {
		  document.insertString(document.getLength(), "Ignored", labelStyle);
		} catch (Exception badLocationException) {
		  System.err.println("Oops");
		}
		return this;
	}
	
	public Console addImage(Icon icon){
		StyledDocument document = outputs.getStyledDocument();
		
		Style labelStyle = outputs.addStyle("", null);
		
		JLabel label = new JLabel(icon);
		StyleConstants.setComponent(labelStyle, label);
		
		try {
		  document.insertString(document.getLength(), "Ignored", labelStyle);
		} catch (Exception badLocationException) {
		  System.err.println("Oops");
		}
		return this;
	}
	
	public void output(){
		StyledDocument doc = outputs.getStyledDocument();

        Style style = outputs.addStyle("", null);
        StyleConstants.setForeground(style, Color.BLACK);

        try { doc.insertString(doc.getLength(), "\n",style); }
        catch (Exception e){}
	}
	
	public String getLastInput(){
		
		return lastInput;
		
	}
}