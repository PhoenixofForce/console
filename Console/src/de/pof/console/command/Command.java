package de.pof.console.command;

import de.pof.console.CConsole;
import de.pof.console.JCConsole;

public interface Command {

	public String getCommandLine();
	public String getDescription();
	
	public void onCommand(CConsole c);
	public void onCommand(JCConsole c);
}
